// Loading Libary 
//-----------------------------------
#include <SFML/Graphics.hpp>


// Worms By Scott Gilbert 02.10.21
int main()
{
	// Setting up Game window , 
	sf::RenderWindow gameWindow;
	gameWindow.create(sf::VideoMode(600, 360), "Worms By Scott Gilbert",sf::Style::Titlebar|sf::Style::Close);
	
	// -------------------------------
	//  Game Setup Start up Section
	// -------------------------------


	// Set up  Key press
	gameWindow.setKeyRepeatEnabled(false);
	
	// Setup Game Clock 
	sf::Clock gameClock;


	// Setting up Player Sprite
	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");

	sf::Sprite playerSprite;
	playerSprite.setTexture(playerTexture);
	
	// Setup Starting loc 
	sf::Vector2f playerPos(150,237);
	playerSprite.setPosition(playerPos);

	


	// BackGound img ------------------------
	sf::Texture bgTexture;
	bgTexture.loadFromFile("Assets/Graphics/BG.jpg");
	sf::Sprite BGImg;
	BGImg.setTexture(bgTexture);
	BGImg.setPosition(gameWindow.getSize().x / 2 - bgTexture.getSize().x / 2,
	gameWindow.getSize().y / 2 - bgTexture.getSize().y / 2);


	// -------------------------------
	//  Game Loop
	// -------------------------------

	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
	    //  Input Section 
	    // -----------------------------------------------

		// Setting up movment Vars 
		sf::Vector2f velocity;
		float speed; 
		float gravity;
		
		
		
		sf::Vector2f previousPosition;

		speed = 300.0f;
		gravity = 400.0f;
		
		
		
		
		

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			// Move player left
			velocity.x = -speed;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			// Move player right
			velocity.x = speed;
		}


			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				

					const float JUMP_VALUE = -900; // negative to go up. Adjust as needed.
					velocity.y = JUMP_VALUE;
			}

			
			
			velocity.y = velocity.y + gravity;

			
			

			

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
			

		sf::Time frameTime = gameClock.restart();
		previousPosition = playerSprite.getPosition();

		

		// Calculate the new position
		sf::Vector2f newPosition = playerSprite.getPosition() + velocity * frameTime.asSeconds();

		// Calculate The Grav 
		//calculate the new velocity
		
		

		playerSprite.setPosition(newPosition);
		

		
		if (newPosition.y + playerSprite.getTexture()->getSize().y > gameWindow.getSize().y -100 )
		{
			newPosition.y = gameWindow.getSize().y -100 - playerSprite.getTexture()->getSize().y;
			playerSprite.setPosition(newPosition);
			
		}
		

		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(85, 30, 138));
		// Drawing BackGound Image 
		gameWindow.draw(BGImg);

		gameWindow.draw(playerSprite);

		gameWindow.display();

	} // End of Game loop
	

	return 0;
}